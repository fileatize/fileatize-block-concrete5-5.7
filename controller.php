<?php
namespace Concrete\Package\Fileatize;
use Package;
use BlockType;
use Route;

class Controller extends Package {

	protected $pkgHandle = 'fileatize';
	protected $appVersionRequired = '5.7.3';
	protected $pkgVersion = '0.9.0';

	public function getPackageName() {
		return t("Fileatize");
	}
	
	public function getPackageDescription() {
		return t("Sell digital Files your concrete5 website");
	}
	
	public function on_start() {
		Route::register('/fileatize/fileatize', '\Concrete\Package\Fileatize\Controller\Fileatize\Fileatize');
	}	
 
	public function install() {
		$pkg = parent::install();
		
		// Install Block
		if(!BlockType::getByHandle('fileatize_button')) {
			BlockType::installBlockTypeFromPackage('fileatize_button', $pkg);
		}

		//if(!BlockType::getByHandle('fileatize_product')) {
		//	BlockType::installBlockTypeFromPackage('fileatize_product', $pkg);
		//}
		
	}
}
?>