/* File Links */
refreshfileLocationControls = function() {
	var fileLocation = $('#fileLocation').val();
	$('#fileLocationInternal').toggle(fileLocation == 1);
	$('#fileLocationExternal').toggle(fileLocation == 2);
}

$(document).ready(function() {
	$('#fileLocation').change(refreshfileLocationControls);
	refreshfileLocationControls();
});