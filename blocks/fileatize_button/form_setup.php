<?php defined('C5_EXECUTE') or die(_("Access Denied."));
$al = Loader::helper('concrete/asset_library');
$bf = null;
if ($controller->getFileID() > 0) {
    $bf = $controller->getFileObject();
}
?>

<div class="form-group">
	<?php echo $form->label('productName', t('Product Name'))?>
	<?php echo $form->text('productName', $productName)?>
</div>

<div class="form-group">
	<?php echo $form->label('currency', t('Currency'))?>
	<?php echo $form->select('currency', array('USD'=>t('&#36; (Dollar)'), 'GBP'=>t('&#163; (Pound)'), 'EUR'=>t('&#8364; (Euro)')), $currency);?>
</div>

<div class="form-group">
	<?php echo $form->label('price', t('Price'))?>
	<?php echo $form->text('price', $price)?>
</div>

<div class="form-group">
	<?php echo $form->label('fileLocation', t('File'))?>
	<select name="fileLocation" id="fileLocation" class="form-control">
		<option value="0" <?php echo (empty($fileatizeExternalFile) && empty($fileatizeInternalFile) ? 'selected="selected"' : '')?>><?php echo t('None')?></option>
		<option value="1" <?php echo (empty($fileatizeExternalFile) && !empty($fileatizeInternalFile) ? 'selected="selected"' : '')?>><?php echo t('File Manager')?></option>
		<option value="2" <?php echo (!empty($fileatizeExternalFile) ? 'selected="selected"' : '')?>><?php echo t('External URL')?></option>
	</select>
</div>
<div id="fileLocationInternal" style="display: none;" class="form-group">	
	<?php echo $form->label('fileatizeInternalFile', t('File Manager'))?>
	<?php echo $al->file('ccm-b-file', 'fileatizeInternalFile', t('Choose File'), $bf); ?>
</div>			
<div id="fileLocationExternal" style="display: none;" class="form-group">
	<?php echo $form->label('fileatizeExternalFile', t('URL'))?>
	<?php echo $form->text('fileatizeExternalFile', $fileatizeExternalFile); ?>
</div>

<div class="form-group">
	<?php echo $form->label('emailAddress', t('Email Address'))?>
	<?php echo $form->text('emailAddress', $emailAddress)?>
</div>

<div class="form-group">
	<?php echo $form->label('buttonText', t('Button Text'))?>
	<?php echo $form->text('buttonText', $buttonText)?>
</div>

<div class="form-group">
	<?php echo $form->label('maxWidth', t('Max Width'))?>
	<?php echo $form->text('maxWidth', $maxWidth)?>
</div>