<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>

<?php
if (strpos($fileatizeSnippet, 'http') === 0) { ?>

<div class="fltz-button-wrapper">
<?php echo '<a href="'.$fileatizeSnippet.'" target="'.$target.'">';?>
	<button class="fltz-button<?php echo $shadow;?>"<?php echo $buttonStyles;?>>
		<span<?php echo $productButtonColor;?>>
			<?php echo $buttonText;?>
		</span>
		<span class="fltz-button-price"<?php echo $productPriceColor;?>>
			<?php echo $currency.$price;?>
		</span>
	</button><!-- END .fltz-product-button -->
<?php echo '</a>';?>
<?php echo $paypalLogo;?>
</div>

<?php
} else {
	echo '<div class="fltz-error">';
	echo $fileatizeSnippet;
	echo '</div>';
}
?>